//Jquery functions
jQuery(document).ready(function ($) {


//Editable fields function
	var defaults = {
    mode: 'popup',
		placement: 'right',
    toggle: 'manual',
    showbuttons: true,
    onblur: 'cancel',
    savenochange: true,
		clear: false

};
$.extend($.fn.editable.defaults, defaults);

$('#user-details span[data-name="name"]').editable({
    title: 'Enter name'
});
$('#user-details span[data-name="web"]').editable({
    title: 'Enter Web Url'
});
$('#user-details span[data-name="phone"]').editable({
    title: 'Enter Phone Number'
});
$('#user-details span[data-name="address"]').editable({
    title: 'Enter Address'
});

$('#user-details').on('click', '.edit', function(){
    $('#user-details').find('.editable-open').editable('hide');
    $('#user-details').find('.edit').show();
    $(this).closest('tr').find('.editable').editable('show');
});

$('#user-details').on('click', '.btn-primary', function() {
    var $btn = $(this);
    $btn.closest('tr').find('.editable').editable('hide');
    $btn.hide().siblings('.edit').show();
});

//Dynamic views function
	var views = {
			profile: [{
							selector: "#header",
							templateUrl: 'include/header.html'
					},
			],

			defaultView: {
					view: 'profile'
			}
	};

	new Via(views);


//Menu touch function

	/*
	function is_touch_device()
	{
	  // Checks for existence in all browsers and IE10/11 & Surface
	  return 'ontouchstart' in window || navigator.maxTouchPoints;
	}
  */

	var navs = document.querySelectorAll('.scroll');

	for ( var i = 0, length = navs.length; i < length; i++ ) {
	    var nav = navs[i];
	    new Flickity( nav, {
	      cellAlign: 'left',
	      freeScroll: false,
	      pageDots: false,
	      contain: true
	    });
	  }


//Function for mobile toggle display user details

$("#edit-buttons").hide();
$("#mobile-profile-edit").hide();

$("#mobile-edit").click(function(){
			 $("#user-details").hide();
			 $("#mobile-edit").hide();
			 $("#edit-buttons").show();
			 $("#mobile-profile-edit").show();
	 });
$("#edit-buttons").click(function(){
	 			$("#user-details").show();
	 			$("#mobile-edit").show();
	 			$("#edit-buttons").hide();
				$("#mobile-profile-edit").hide();
	 	 });

});
