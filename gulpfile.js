var gulp = require('gulp'),
	bower = require('gulp-bower'),
	sass = require('gulp-sass'),
	autoprefix = require('gulp-autoprefixer'),
	uglify = require('gulp-uglifyjs'),
	notify = require('gulp-notify'),
	browserSync = require('browser-sync').create();

var config = {
	bowerDir: './bower_components',
	srcDir: './src',
	distDir: './dist'
};

gulp.task('bower', function () {
	return bower()
		.pipe(gulp.dest(config.bowerDir))
});

gulp.task('css', function () {
	return gulp.src('src/css/app.scss')
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.on('error', notify.onError(function (error) {
			return 'Error: ' + error.message;
		}))
		.pipe(gulp.dest(config.distDir + '/css'))
		.pipe(browserSync.stream());
});

gulp.task('ionicons', function () {
	return gulp.src(config.bowerDir + '/ionicons/scss/ionicons.scss')
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.on('error', notify.onError(function (error) {
			return 'Error: ' + error.message;
		}))
		.pipe(gulp.dest(config.distDir + '/css'))
		.pipe(browserSync.stream());
});

gulp.task('fonts', function () {
	gulp.src(config.bowerDir + '/ionicons/fonts/*')
		.pipe(gulp.dest(config.distDir + '/fonts'));
});

gulp.task('images', function () {
	gulp.src('src/images/*')
		.pipe(gulp.dest(config.distDir + '/images'));
});

gulp.task('js', function () {
	return gulp.src([
			config.bowerDir + '/jquery/dist/jquery.js',
			config.bowerDir + '/poshytip/src/jquery.poshytip.js',
			config.bowerDir + '/x-editable/dist/jquery-editable/js/jquery-editable-poshytip.js',
			config.bowerDir + '/flickity/dist/flickity.pkgd.js',
			config.srcDir + '/js/viajs.js',
			config.srcDir + '/js/app.js'
		])
		.pipe(uglify('app.js', {
			compress: true,
			outSourceMap: false,
		}))
		.pipe(gulp.dest(config.distDir + '/js'));
});

gulp.task('js-watch', ['js'], browserSync.reload);

gulp.task('html', function () {
	gulp.src(config.srcDir + '/*.html')
		.pipe(gulp.dest(config.distDir));
});

gulp.task('include', function () {
	gulp.src('src/include/*')
		.pipe(gulp.dest(config.distDir + '/include'));
});

gulp.task('serve', ['css'], function () {
	browserSync.init({
		server: config.distDir
	});
	gulp.watch(config.bowerDir + '/**/*.scss', ['css']);
	gulp.watch(config.srcDir + '/**/*.scss', ['css']);
	gulp.watch(config.srcDir + '/*.html', ['html']).on('change', browserSync.reload);
	gulp.watch(config.srcDir + '/include/*.html', ['html']).on('change', browserSync.reload);
	gulp.watch(config.srcDir + '/**/*.js', ['js-watch']);
});

gulp.task('default', ['css', 'ionicons', 'fonts', 'js', 'html', 'include', 'images', 'serve']);
